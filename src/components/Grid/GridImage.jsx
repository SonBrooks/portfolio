import React, { Component } from 'react';

export default class GridImage extends Component {
    constructor(props) {
        super(props);

        ///TODO MAKE HOVER DROP SHADOW EFFECT
    }

    render() {
        const { itemLink, itemAlt, itemClassName, imageURL } = this.props
        return (
            <a href={itemLink}>
                <img
                    alt={itemAlt}
                    src={imageURL}
                    className={itemClassName}
                />
            </a>
        )
    }

}